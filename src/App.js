import logo from "./logo.svg";
import "./App.css";
import AddIcon from "@mui/icons-material/Add";
import ReorderIcon from "@mui/icons-material/Reorder";
import SsidChartIcon from "@mui/icons-material/SsidChart";
import Table from "./components/table/table";
import { useState } from "react";
import { Sidebar } from 'primereact/sidebar';
import 'primeicons/primeicons.css';
import 'primereact/resources/themes/lara-light-indigo/theme.css';
import 'primereact/resources/primereact.css';
import 'primeflex/primeflex.css';
import Form from "./components/form/form";


function App() {
  const [displayform,setdisplayform]=useState(false)

  function displaysidebar()  {
      setdisplayform(true)
  }
  return (
    <div className="app">
      <Sidebar visible={displayform} position="right" onHide={() =>setdisplayform(false)}>
                        < Form/>
      </Sidebar>
      <div className="header-section">
        <p className="heading">Vehical List</p>
        <button className="button-element" onClick={displaysidebar}>
          <AddIcon />
          Add Vehicle
        </button>
        <span className="flex-grow"></span>
        <div className="view-section">
          <ReorderIcon />
          <SsidChartIcon />
        </div>
      </div>
    <Table/>
      
    </div>
  );
}

export default App;
