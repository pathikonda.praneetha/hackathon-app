import React from 'react'
import AddIcon from "@mui/icons-material/Add";

export default function Form() {
  return (

    <form className="add-vehicle-form">
      <p  className="add-vehicle-heading">Add a Vehicle</p>
      <p><span className="req">*</span> All fields are mandatory</p>
      <label  className="label-text" for="name">
        Name
        <input  className="input-text" type="text" name="name" id="name" placeholder="SELECT"/>
      </label>
      <br/>
      <label  className="label-text">
        Manufacturer
        <input className="input-text" type="text" name="name"  placeholder="SELECT"/>
      </label >
      <br/>
      <label  className="label-text">
        Model
        <input className="input-text" type="text" name="name" placeholder="SELECT"/>
      </label>
      <br/>
      <label  className="label-text">
        Fuel
        <input className="input-text" type="text" name="name" placeholder="SELECT"/>
      </label>
      <br/>
      <label  className="label-text">
        Colour
        <input className="input-text" type="text" name="name" placeholder="SELECT"/>
      </label>
      <br/>
      <label  className="label-text">
        Price
        <input  className="input-text"type="text" name="name" placeholder="ENTER"/>
      </label>
      <br/>
      <label  className="label-text">
        Currency
        <input  className="input-text" type="text" name="name" placeholder="SELECT"/>
      </label>
      <br/>
      <label  className="label-text">
        City
        <input className="input-text" type="text" name="name" placeholder="SELECT"/>
      </label>
      <br/>
      <label  className="label-text">
        Country
        <input className="input-text" type="text" name="name" placeholder="SELECT"/>
      </label>
      <br/>
      <div className="buttons-container">
      <button className="button-element">
      <AddIcon />
      Add Vehicle
       </button>
    <button className="cancel-button">Cancel</button>
      </div>
     
    </form>
  
  )
}
