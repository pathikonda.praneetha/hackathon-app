import React from 'react'

export default function Table() {
  return (
    <div className="table-section">
    <table>
      <tbody>
        <tr>
          <th className="check">
            <input type="checkbox" />
          </th>
          <th className="name">NAME</th>
          <th className="table-heads">MANUFACTURER</th>
          <th className="table-heads">MODEL</th>
          <th className="table-heads">FUEL</th>
          <th className="table-heads">COLOUR</th>
          <th className="table-heads">PRICE</th>
          <th className="table-heads">CURRENCY</th>
          <th className="table-heads">CITY</th>
          <th className="table-heads">COUNTRY</th>
        </tr>

        <tr>
          <td className="check-details">
            <input type="checkbox" />
          </td>
          <td className="name-details">name</td>
          <td className="table-data">manufacturer</td>
          <td className="table-data">model</td>
          <td className="table-data">fuel</td>
          <td className="table-data">colour</td>
          <td className="table-data">price</td>
          <td className="table-data">currency</td>
          <td className="table-data">city</td>
          <td className="table-data">country</td>
        </tr>
      </tbody>
    </table>
  </div>
  )
}
